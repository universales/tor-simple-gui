# Tor Simple GUI

Tor Simple GUI is a simple interface for Tor.<br>
The graphical interface eliminates the need to use commands.<br><br>

**How to run the graphical interface.**<br><br>
1 - Download [TORProxy.sh](https://gitlab.com/universales/tor-simple-gui/-/raw/master/TORProxy.sh?inline=false)<br>
2 - Run it through the terminal 'sh TORProxy.sh'<br><br>
All ready :)<br><br>

**It is recommended to add execution permission to the script although it is not necessary.**

**Changelog**

**V0.1** - Initial version.