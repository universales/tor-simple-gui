#!/bin/bash

inicio(){
opcion=$(whiptail --title "Tor Minimal GUI | V0.1" --menu "Select an option in the menu:" 15 60 3 \
"1" "Initialize Tor proxy server." \
"2" "Restart Tor proxy server." \
"3" "Stop Tor proxy server."  3>&1 1>&2 2>&3)
 
exitstatus=$?
if [ $exitstatus = 0 ]; then    
    if [ $opcion = "1" ]; then
       sudo systemctl start tor;
       mensajeinfo "Done...";
       inicio;
    fi
    if [ $opcion = "2" ]; then
       sudo systemctl restart tor;
       mensajeinfo "Done...";
       inicio;
    fi
    if [ $opcion = "3" ]; then
       sudo systemctl stop tor;
       mensajeinfo "Done...";
       inicio;
    fi
else
    exit 1;
fi
}


mensajeinfo(){
whiptail --title "Tor Minimal GUI | V0.1" --msgbox "$1" 30 60
}


if [ -n "$(type protonvpn)" ]; then
    inicio;
else
    mensajeinfo "Tor is not installed on your system.";
fi
exit 1;
